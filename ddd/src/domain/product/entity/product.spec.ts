import Product from './product';

describe('Product unit tests', () => {
  it('should throw error when id is empty', () => {
    let product = () => new Product('', 'Shirt', 491.0);

    expect(product).toThrowError('Id is required');
  });

  it('should throw error when name is empty', () => {
    let product = () => new Product('item_id', '', 491.0);

    expect(product).toThrowError('Name is required');
  });

  it('should throw error when price is less than zero', () => {
    let product = () => new Product('item_id', 'Gloves', -10);

    expect(product).toThrowError('Price must be greater than zero');
  });

  it('should change name', () => {
    let product = new Product('item_id', 'Gloves', 779.0);
    product.changeName('Pants');

    expect(product.name).toBe('Pants');
  });

  it('should change price', () => {
    let product = new Product('item_id', 'Gloves', 779.0);
    product.changePrice(661.0);

    expect(product.price).toBe(661.0);
  });
});
