import OrderItem from './order_item';

export default class Order {
  private _id: string;
  private _customerId: string;
  private _items: OrderItem[];
  private _total: number;

  constructor(id: string, customerId: string) {
    this._id = id;
    this._customerId = customerId;
    this._items = [];
    this.validate();
  }

  get id(): string {
    return this._id;
  }

  get customerId(): string {
    return this._customerId;
  }

  get items(): OrderItem[] {
    return this._items
  }

  addItems(items: OrderItem[]): Order {
    this._items.push(...items)
    return this
  }

  updateItem(itemId: string, item: OrderItem): void {
    const itemToUpdate = this._items.findIndex((itemFound) => itemFound.id === itemId)
    this._items[itemToUpdate] = item
  }

  validate(): boolean {
    if (this._id.length === 0) {
      throw new Error('Id is required');
    }

    if (this._customerId.length === 0) {
      throw new Error('Customer id is required');
    }

    return true;
  }

  calculateTotal(): number {
    this._total = this._items.reduce((acc, item) => acc + item.orderItemTotal(), 0);
    return this._total;
  }
}
