import Order from './order';
import OrderItem from './order_item';

describe('Order unit tests', () => {
  it('should throw error when id is empty', () => {
    let order = () => new Order('', 'customer_id');

    expect(order).toThrowError('Id is required');
  });

  it('should throw error when customer id is empty', () => {
    let order = () => new Order('order_id', '');

    expect(order).toThrowError('Customer id is required');
  });

  it('should calculate total', () => {


    let order = new Order('order_id', 'customer_id');

    const itemA = new OrderItem('item_a', 'product_id',order.id, 'Salad', 25.0, 2);
    const itemB = new OrderItem('item_b', 'product_id',order.id, 'Soap', 346.0, 4);
    order.addItems([itemA, itemB])

    const total = order.calculateTotal();

    expect(total).toBe(1434);
  });
});
