import Customer from '../../customer/entity/customer';
import Order from '../entity/order';
import OrderItem from '../entity/order_item';
import OrderService from './order.service';

describe('Order service unit tests', () => {
  it('should place an order', () => {
    const priceItem1 = 122.0;
    const qtdItem1 = 2;

    const expectRewardPoints = priceItem1 * qtdItem1 * 0.5;

    const customer = new Customer('client_1', 'Otha');
    const item1 = new OrderItem(
      'item_1',
      'product_1',
      'order_id',
      'Chicken',
      priceItem1,
      qtdItem1,
    );

    const order = OrderService.placeOrder(customer, [item1]);

    expect(customer.rewardPoints).toBe(expectRewardPoints);
    expect(order.calculateTotal()).toBe(priceItem1 * qtdItem1);
  });

  it('should get total of all orders', () => {
    const priceItem1 = 728.0;
    const priceItem2 = 135.0;

    const qtdItem1 = 2;
    const qtdItem2 = 1;

    const orderItem1 = new OrderItem(
      'item_1',
      'product_id',
      'order_id',
      'Chips',
      priceItem1,
      qtdItem1,
    );
    const orderItem2 = new OrderItem(
      'item_2',
      'product_id',
      'order_id',
      'Car',
      priceItem2,
      qtdItem2,
    );

    const order1 = new Order('order_1', 'client_1');
    order1.addItems([orderItem1]);
    const order2 = new Order('order_2', 'client_1');
    order2.addItems([orderItem2]);

    const total = OrderService.total([order1, order2]);

    expect(total).toBe(priceItem1 * qtdItem1 + priceItem2 * qtdItem2);
  });
});
