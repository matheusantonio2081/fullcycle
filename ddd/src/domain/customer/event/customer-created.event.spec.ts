import EventDispatcher from '../../@shared/event/event-dispatcher';
import CustomerCreatedEvent from './customer-created.event';
import SendConsoleLog1WhenCustomerIsCreatedHandler from './handler/send-console-log1-when-customer-is-created.handler';
import SendConsoleLog2WhenCustomerIsCreatedHandler from './handler/send-console-log2-when-customer-is-created.handler';

describe('Customer Created Event', () => {
  it('should print message in log one when customer is created', () => {
    const eventDispatcher = new EventDispatcher();
    const eventHandler = new SendConsoleLog1WhenCustomerIsCreatedHandler();

    const spyEventHandler = jest.spyOn(eventHandler, 'handle');

    eventDispatcher.register('CustomerCreatedEvent', eventHandler);

    const customerCreatedEvent = new CustomerCreatedEvent({
      name: "D'angelo",
      age: 12,
    });

    eventDispatcher.notify(customerCreatedEvent);

    expect(spyEventHandler).toHaveBeenCalled();
  });

  it('should print message in log two when customer is created', () => {
    const eventDispatcher = new EventDispatcher();
    const eventHandler = new SendConsoleLog2WhenCustomerIsCreatedHandler();

    const spyEventHandler = jest.spyOn(eventHandler, 'handle');

    eventDispatcher.register('CustomerCreatedEvent', eventHandler);

    const customerCreatedEvent = new CustomerCreatedEvent({
      name: "D'angelo",
      age: 12,
    });

    eventDispatcher.notify(customerCreatedEvent);

    expect(spyEventHandler).toHaveBeenCalled();
  });
});
