import Address from '../value-object/address';
import Customer from './customer';

describe('Customer', () => {
  it('should throw error when id is empty', () => {
    let customer = () => new Customer('', 'Viviane');

    expect(customer).toThrowError('Id is required');
  });

  it('should throw error when name is empty', () => {
    let customer = () => new Customer('customer_1', '');

    expect(customer).toThrowError('Name is required');
  });

  it('should change name', () => {
    let customer = new Customer('customer_1', 'Jonatan');

    customer.changeName('Gilbert');

    expect(customer.name).toBe('Gilbert');
  });

  it('should active customer', () => {
    let customer = new Customer('customer_1', 'Jonatan');
    const address = new Address(
      'Rossiemouth',
      123,
      '12344-123',
      'Nicholasview',
    );
    customer.upsertAddress(address);

    customer.activate();

    expect(customer.isActive()).toBe(true);
  });

  it('should deactivate customer', () => {
    let customer = new Customer('customer_1', 'Jonatan');

    customer.deactivate();

    expect(customer.isActive()).toBe(false);
  });

  it('should throw error when address is undefined when you activate a customer', () => {
    let customer = new Customer('customer_1', 'Holly');

    const active = () => customer.activate();

    expect(active).toThrowError('Address is mandatory to activate a customer');
  });

  it('should add reward points', () => {
    const customer = new Customer('customer_id', 'Abelardo');
    expect(customer.rewardPoints).toBe(0);

    customer.addRewardPoint(10);
    expect(customer.rewardPoints).toBe(10);

    customer.addRewardPoint(10);
    expect(customer.rewardPoints).toBe(20);
  });
});
