import OrderItem from '../../../domain/checkout/entity/order_item';
import OrderItemRepositoryInterface from '../../../domain/checkout/repository/order-item-repository.interface';
import OrderItemModel from '../sequelize/order-item.model';

export default class OrderItemRepository
  implements OrderItemRepositoryInterface
{
  async save(entity: OrderItem): Promise<void> {
    await OrderItemModel.create({
      id: entity.id,
      product_id: entity.productId,
      order_id: entity.orderId,
      name: entity.name,
      price: entity.price,
      quantity: entity.quantity,
    });
  }
  async update(entity: OrderItem): Promise<void> {
    await OrderItemModel.update(
      {
        id: entity.id,
        productId: entity.productId,
        name: entity.name,
        price: entity.price,
        quantity: entity.quantity,
      },
      {
        where: {
          id: entity.id,
        },
      },
    );
  }
  async find(id: string): Promise<OrderItem> {
    const orderItemModel = await OrderItemModel.findOne({ where: { id } });

    return new OrderItem(
      orderItemModel.id,
      orderItemModel.product_id,
      orderItemModel.order_id,
      orderItemModel.name,
      orderItemModel.price,
      orderItemModel.quantity,
    );
  }

  async findAll(): Promise<OrderItem[]> {
    const orderItemModels = await OrderItemModel.findAll();
    const orderItems = orderItemModels.map((orderItem) => {
      return new OrderItem(
        orderItem.id,
        orderItem.product_id,
        orderItem.order_id,
        orderItem.name,
        orderItem.price,
        orderItem.quantity,
      );
    });
    return orderItems;
  }
}
