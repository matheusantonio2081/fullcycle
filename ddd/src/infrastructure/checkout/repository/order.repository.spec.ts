import { Sequelize } from 'sequelize-typescript';
import Order from '../../../domain/checkout/entity/order';
import OrderItem from '../../../domain/checkout/entity/order_item';
import Customer from '../../../domain/customer/entity/customer';
import Address from '../../../domain/customer/value-object/address';
import Product from '../../../domain/product/entity/product';
import CustomerRepository from '../../customer/repository/customer.repository';
import CustomerModel from '../../customer/sequelize/customer.model';
import ProductRepository from '../../product/repository/product.repository';
import ProductModel from '../../product/sequelize/product.model';
import OrderItemModel from '../sequelize/order-item.model';
import OrderModel from '../sequelize/order.model';
import OrderItemRepository from './order-item.repository';
import OrderRepository from './order.repository';

describe('Customer repository test', () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false,
      sync: { force: true },
    });
    sequelize.addModels([
      CustomerModel,
      OrderModel,
      OrderItemModel,
      ProductModel,
    ]);
    await sequelize.sync();
  });
  afterEach(async () => {
    await sequelize.close();
  });

  it('should create a new order', async () => {
    const productRepository = new ProductRepository();
    const product = new Product('product_id', 'Sausages', 350.0);
    await productRepository.save(product);

    const customerRepository = new CustomerRepository();
    const customer = new Customer('customer_id', 'Ramon');
    const address = new Address('Adeline Burgs', 32, '35175', 'Jacobiton');
    customer.upsertAddress(address);
    await customerRepository.save(customer);

    const order = new Order('order_id', customer.id);
    const orderRepository = new OrderRepository();
    await orderRepository.save(order);

    const orderItemRepository = new OrderItemRepository();
    const orderItem = new OrderItem(
      'order_item_id',
      product.id,
      order.id,
      product.name,
      product.price,
      2,
    );
    await orderItemRepository.save(orderItem);
    order.addItems([orderItem]);
    order.calculateTotal();
    orderRepository.update(order);

    const orderModel = await OrderModel.findOne({
      where: { id: order.id },
      include: ['items'],
    });

    expect(orderModel.toJSON()).toStrictEqual({
      id: order.id,
      customer_id: customer.id,
      total: order.calculateTotal(),
      items: [
        {
          id: orderItem.id,
          name: orderItem.name,
          price: orderItem.price,
          order_id: order.id,
          product_id: orderItem.productId,
          quantity: orderItem.quantity,
        },
      ],
    });
  });

  it('should update order', async () => {
    const customerRepository = new CustomerRepository();
    const customer = new Customer('customer_id', 'Ramon');
    const address = new Address('Adeline Burgs', 32, '35175', 'Jacobiton');
    customer.upsertAddress(address);
    await customerRepository.save(customer);

    const orderRepository = new OrderRepository();
    const order = new Order('order_id', customer.id);
    await orderRepository.save(order);

    const productRepository = new ProductRepository();
    const product1 = new Product('product_id_1', 'Sausages', 350.0);
    await productRepository.save(product1);
    const orderItemRepository = new OrderItemRepository();
    const orderItem1 = new OrderItem(
      'order_item_id_1',
      product1.id,
      order.id,
      product1.name,
      product1.price,
      2,
    );
    await orderItemRepository.save(orderItem1);

    const product2 = new Product('product_id_2', 'Tuna', 230.0);
    await productRepository.save(product2);
    const orderItem2 = new OrderItem(
      'order_item_id_2',
      product2.id,
      order.id,
      product2.name,
      product2.price,
      1,
    );
    await orderItemRepository.save(orderItem2);

    order.addItems([orderItem2]);
    order.calculateTotal();

    await orderRepository.update(order);

    const orderModel = await OrderModel.findOne({
      where: { id: order.id },
      include: ['items'],
    });

    expect(orderModel.toJSON()).toStrictEqual({
      id: order.id,
      customer_id: customer.id,
      total: order.calculateTotal(),
      items: [
        {
          id: orderItem1.id,
          name: orderItem1.name,
          price: orderItem1.price,
          order_id: order.id,
          product_id: orderItem1.productId,
          quantity: orderItem1.quantity,
        },
        {
          id: orderItem2.id,
          name: orderItem2.name,
          price: orderItem2.price,
          order_id: order.id,
          product_id: orderItem2.productId,
          quantity: orderItem2.quantity,
        },
      ],
    });
  });
});
