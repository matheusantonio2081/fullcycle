import Order from '../../../domain/checkout/entity/order';
import OrderRepositoryInterface from '../../../domain/checkout/repository/order-repository.interface';
import OrderItemModel from '../sequelize/order-item.model';
import OrderModel from '../sequelize/order.model';

export default class OrderRepository implements OrderRepositoryInterface {
  async update(entity: Order): Promise<void> {
    const items = entity.items.map((item) => ({
      id: item.id,
      product_id: item.productId,
      name: item.name,
      price: item.price,
      quantity: item.quantity,
    }));

    await OrderModel.update(
      {
        id: entity.id,
        customer_id: entity.customerId,
        total: entity.calculateTotal(),
        items: items,
      },
      {
        where: {
          id: entity.id,
        },
      },
    );
  }
  find(id: string): Promise<Order> {
    throw new Error('Method not implemented.');
  }
  findAll(): Promise<Order[]> {
    throw new Error('Method not implemented.');
  }
  async save(entity: Order): Promise<void> {
    await OrderModel.create(
      {
        id: entity.id,
        customer_id: entity.customerId,
        total: 0,
        items: [],
      },
      {
        include: [{ model: OrderItemModel }],
      },
    );
  }
}
