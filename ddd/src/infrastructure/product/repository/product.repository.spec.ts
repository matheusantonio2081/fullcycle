import { Sequelize } from 'sequelize-typescript';
import Product from '../../../domain/product/entity/product';
import ProductModel from '../sequelize/product.model';
import ProductRepository from './product.repository';

describe('Product repository test', () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false,
      sync: { force: true },
    });
    sequelize.addModels([ProductModel]);
    await sequelize.sync();
  });
  afterEach(async () => {
    await sequelize.close();
  });

  it('should create a product', async () => {
    const product = new Product('product_id', 'Bike', 100);

    new ProductRepository().save(product);

    const productModel = await ProductModel.findOne({
      where: { id: 'product_id' },
    });

    expect(productModel.toJSON()).toStrictEqual({
      id: 'product_id',
      name: 'Bike',
      price: 100,
    });
  });

  it('should update a product', async () => {
    const product = new Product('product_id', 'Bike', 100);

    const productRepository = new ProductRepository();

    await productRepository.save(product);

    const productModel = await ProductModel.findOne({
      where: { id: 'product_id' },
    });

    expect(productModel.toJSON()).toStrictEqual({
      id: 'product_id',
      name: 'Bike',
      price: 100,
    });

    product.changeName('Chair');
    product.changePrice(200);

    await productRepository.update(product);

    const productModel2 = await ProductModel.findOne({
      where: { id: 'product_id' },
    });

    expect(productModel2.toJSON()).toStrictEqual({
      id: 'product_id',
      name: 'Chair',
      price: 200,
    });
  });

  it('should find a product', async () => {
    const product = new Product('product_id', 'Bike', 100);
    const productRepository = new ProductRepository();

    await productRepository.save(product);

    const productModel = await ProductModel.findOne({
      where: { id: 'product_id' },
    });

    const foundProduct = await productRepository.find('product_id');

    expect(productModel.toJSON()).toStrictEqual({
      id: foundProduct.id,
      name: foundProduct.name,
      price: foundProduct.price,
    });
  });

  it('should find a product', async () => {
    const product1 = new Product('product_id_1', 'Bike', 100);
    const product2 = new Product('product_id_2', 'Tuna', 100);
    const productRepository = new ProductRepository();

    await productRepository.save(product1);
    await productRepository.save(product2);

    const foundProducts = await productRepository.findAll();
    const products = [product1, product2];

    expect(foundProducts).toEqual(products);
  });
});
