import Customer from '../../../domain/customer/entity/customer';
import CustomerRepositoryInterface from '../../../domain/customer/repository/customer-repository.interface';
import Address from '../../../domain/customer/value-object/address';
import CustomerModel from '../sequelize/customer.model';

export default class CustomerRepository implements CustomerRepositoryInterface {
  async save(entity: Customer): Promise<void> {
    await CustomerModel.create({
      id: entity.id,
      name: entity.name,
      street: entity.address.street,
      number: entity.address.number,
      zipcode: entity.address.zip,
      city: entity.address.city,
      active: entity.isActive(),
      rewardPoints: entity.rewardPoints,
    });
  }
  async update(entity: Customer): Promise<void> {
    await CustomerModel.update(
      {
        name: entity.name,
        street: entity.address.street,
        number: entity.address.number,
        zipcode: entity.address.zip,
        city: entity.address.city,
        active: entity.isActive(),
        rewardPoints: entity.rewardPoints,
      },
      {
        where: {
          id: entity.id,
        },
      },
    );
  }
  async find(id: string): Promise<Customer> {
    const customerModel = await CustomerModel.findOne({ where: { id } });

    return new Customer(customerModel.id, customerModel.name);
  }
  async findAll(): Promise<Customer[]> {
    const customerModels = await CustomerModel.findAll();
    const customers = customerModels.map((model) => {
      let customer = new Customer(model.id, model.name);
      customer.addRewardPoint(model.rewardPoints);
      const address = new Address(
        model.street,
        model.number,
        model.zipcode,
        model.city,
      );
      customer.upsertAddress(address);

      if (model.active) customer.activate();
      return customer;
    });

    return customers;
  }
}
