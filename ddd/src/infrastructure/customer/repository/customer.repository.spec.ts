import { Sequelize } from 'sequelize-typescript';
import Customer from '../../../domain/customer/entity/customer';
import Address from '../../../domain/customer/value-object/address';
import CustomerModel from '../sequelize/customer.model';
import CustomerRepository from './customer.repository';

describe('Customer repository test', () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false,
      sync: { force: true },
    });
    sequelize.addModels([CustomerModel]);
    await sequelize.sync();
  });
  afterEach(async () => {
    await sequelize.close();
  });

  it('should create a customer', async () => {
    const customer = new Customer('customer_id', 'Tessie');
    const address = new Address(
      'Corwin Heights',
      123,
      '64539',
      'New Dallinborough',
    );
    customer.upsertAddress(address);

    new CustomerRepository().save(customer);

    const customerModel = await CustomerModel.findOne({
      where: { id: 'customer_id' },
    });

    expect(customerModel.toJSON()).toStrictEqual({
      id: 'customer_id',
      name: customer.name,
      active: customer.isActive(),
      rewardPoints: customer.rewardPoints,
      street: customer.address.street,
      number: customer.address.number,
      zipcode: customer.address.zip,
      city: customer.address.city,
    });
  });

  it('should update a customer', async () => {
    const customer = new Customer('customer_id', 'Tessie');
    const address = new Address(
      'Corwin Heights',
      123,
      '64539',
      'New Dallinborough',
    );
    customer.upsertAddress(address);

    const customerRepository = new CustomerRepository();

    await customerRepository.save(customer);

    const customerModel = await CustomerModel.findOne({
      where: { id: 'customer_id' },
    });

    expect(customerModel.toJSON()).toStrictEqual({
      id: 'customer_id',
      name: customer.name,
      active: customer.isActive(),
      rewardPoints: customer.rewardPoints,
      street: customer.address.street,
      number: customer.address.number,
      zipcode: customer.address.zip,
      city: customer.address.city,
    });

    customer.changeName('Eudora');
    customer.upsertAddress(
      new Address('Fritsch Road', 321, '10099', 'Leliatown'),
    );

    await customerRepository.update(customer);

    const customerModel2 = await CustomerModel.findOne({
      where: { id: 'customer_id' },
    });

    expect(customerModel2.toJSON()).toStrictEqual({
      id: 'customer_id',
      name: customer.name,
      active: customer.isActive(),
      rewardPoints: customer.rewardPoints,
      street: 'Fritsch Road',
      number: 321,
      zipcode: '10099',
      city: 'Leliatown',
    });
  });

  it('should find a customer', async () => {
    const customer = new Customer('customer_id', 'Tessie');
    const address = new Address(
      'Corwin Heights',
      123,
      '64539',
      'New Dallinborough',
    );
    customer.upsertAddress(address);

    const customerRepository = new CustomerRepository();
    await customerRepository.save(customer);

    const customerModel = await CustomerModel.findOne({
      where: { id: 'customer_id' },
    });

    const foundCustomer = await customerRepository.find('customer_id');

    expect(customerModel.toJSON()).toStrictEqual({
      id: 'customer_id',
      name: foundCustomer.name,
      active: foundCustomer.isActive(),
      rewardPoints: foundCustomer.rewardPoints,
      street: address.street,
      number: address.number,
      zipcode: address.zip,
      city: address.city,
    });
  });
  it('should find a customer', async () => {
    const customer1 = new Customer('customer_id_1', 'Tessie');
    const address1 = new Address(
      'Corwin Heights',
      123,
      '64539',
      'New Dallinborough',
    );
    customer1.upsertAddress(address1);

    const customer2 = new Customer('customer_id_2', 'Greta');
    const address2 = new Address(
      'Corwin Heights',
      123,
      '64539',
      'New Dallinborough',
    );
    customer2.upsertAddress(address2);
    const customerRepository = new CustomerRepository();

    await customerRepository.save(customer1);
    await customerRepository.save(customer2);

    const foundCustomers = await customerRepository.findAll();
    const customers = [customer1, customer2]; //?

    expect(foundCustomers).toEqual(customers);
  });
});
